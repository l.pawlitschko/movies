import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import VueLetterAvatar from "vue-letter-avatar";
import store from "./store";
import VueResponsiveVideoBackgroundPlayer from "vue-responsive-video-background-player";
import VideoBackground from "vue-responsive-video-background-player";

Vue.component("video-background", VideoBackground);
// Registriere das Plugin global
Vue.use(VueResponsiveVideoBackgroundPlayer);
Vue.use(VueLetterAvatar);

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: (h) => h(App),
}).$mount("#app");
