import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userData: {
      username: "",
      email: "",
    },
  },
  mutations: {
    setUserData(state, { username, email }) {
      state.userData = { username, email };
    },
  },
  actions: {
    setUserData({ commit }, { username, email }) {
      commit("setUserData", { username, email });
    },
  },
  getters: {
    getUserData: (state) => state.userData,
  },
});
