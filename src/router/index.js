import Vue from "vue";
import VueRouter from "vue-router";
import Overview from "../views/Overview.vue";
import Account from "../views/Account.vue";
import SavedMovies from "../views/SavedMovies.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Account",
    component: Account,
  },
  {
    path: "/overview",
    name: "Overview",
    component: Overview,
  },
  {
    path: "/saved/movies",
    name: "SavedMovies",
    component: SavedMovies,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
